import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { CalendarioComponent } from './calendario/calendario.component';

import { FullCalendarModule } from '@fullcalendar/angular'; // the main connector. must go first
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin
import timeGridPlugin from '@fullcalendar/timegrid'; // a plugin
import listPlugin from '@fullcalendar/list'; // a plugin
import interactionPlugin from '@fullcalendar/interaction';
import { ClimaComponent } from './clima/clima.component';
import { SuccessCadastroDialogComponent } from './modals/success-cadastro-dialog/success-cadastro-dialog.component'; // a plugin


// register FullCalendar plugins
FullCalendarModule.registerPlugins([
  dayGridPlugin,
  timeGridPlugin,
  listPlugin,
  interactionPlugin
])

@NgModule({
  declarations: [
    NavbarComponent, 
    SidebarComponent, 
    FooterComponent, 
    CalendarioComponent, 
    ClimaComponent, 
    SuccessCadastroDialogComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FullCalendarModule, // register FullCalendar with you app
    MaterialModule
  ],
  exports:[
    NavbarComponent, 
    SidebarComponent, 
    FooterComponent,
    CalendarioComponent,
    ClimaComponent
  ],
  entryComponents: [SuccessCadastroDialogComponent]
})
export class TempleteModule { }
