import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private http : HttpClient) { }

  getPosition(): Promise<any>
  {
    return new Promise((resolve, reject) => {

      navigator.geolocation.getCurrentPosition(resp => {

          resolve({lng: resp.coords.longitude, lat: resp.coords.latitude});
        },
        err => {
          reject(err);
        });
    });

  }

  getLocationDetail(location): Promise<any> {
    return this.http.get(`https://www.metaweather.com/api/location/search/?lattlong=${location.lat},${location.lng}`)
            .toPromise()
            .then()
  }

  getWeather(id): Promise<any> {
    https://www.metaweather.com/api/location/455826/2020/08/14/
    return this.http.get(`https://www.metaweather.com/api/location/${id}/${moment().format("DD/MM/YYYY")}`)
            .toPromise()
            .then()
  }
}
