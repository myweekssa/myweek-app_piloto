import { Component, OnInit } from '@angular/core';
import { DashService } from 'src/app/pages/dashboard/servico/dash.service';
import { AuthService } from 'src/app/pages/login/servico/auth.service';

@Component({
  selector: 'app-clima',
  templateUrl: './clima.component.html',
  styleUrls: ['./clima.component.css']
})
export class ClimaComponent implements OnInit {

  cidade: string;
  temp_C: String;
  icon_Temp: String;

  constructor(
    private authSerive: AuthService,
    private dashService: DashService
  ) { }

  ngOnInit(): void {
    this.buscarClima()
  }

  buscarClima(){
    this.cidade = 'Campinas' //this.authSerive.getAcesso().local;
    this.dashService.getClima(this.cidade).then((data: String)=> {
      if(data != null){
        var splitted = data.split("-");
        
        this.temp_C = splitted[0];
        this.icon_Temp = splitted[1];
      }
    })
  }

}
