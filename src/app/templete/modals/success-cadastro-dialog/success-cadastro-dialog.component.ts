import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-success-cadastro-dialog',
  templateUrl: './success-cadastro-dialog.component.html',
  styleUrls: ['./success-cadastro-dialog.component.css']
})

export class SuccessCadastroDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<SuccessCadastroDialogComponent>,
    private router : Router,) 
  { }

  onNoClick(): void {
    this.dialogRef.close();
    this.router.navigate(['/login']);
  }

  ngOnInit(): void {
  }

}
