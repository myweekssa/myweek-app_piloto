import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from './../../pages/login/servico/auth.service';
import { Acesso } from './../../models/acesso';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    private router : Router, 
    private authSerive: AuthService,
    
  ) { }

  acesso:Acesso
  

  ngOnInit(): void {
    this.acesso = this.authSerive.getAcesso();
  }

  logout(){
    this.authSerive.doLogout().then((data: string) => {
      this.router.navigate(['/login']);
    })
  }

}
