import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';

import { DashService } from './../../pages/dashboard/servico/dash.service';
import { AuthService } from 'src/app/pages/login/servico/auth.service';
import { EventoClienteVO } from 'src/app/models/vo/evento-cliente-vo';
import { Acesso } from 'src/app/models/acesso';

import { CalendarOptions} from '@fullcalendar/angular';
import allLocales from '@fullcalendar/core/locales-all';
import { Cidade } from 'src/app/models/enum/cidade.enum';


@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.component.html',
  styleUrls: ['./calendario.component.css']
})
export class CalendarioComponent implements OnInit{

  constructor(
    private dashService: DashService,
    private authSerive: AuthService,
  ) { }

  cidade = new FormControl();
  cidadeList: string[] =  Object.values(Cidade);

  ngOnInit(): void {
    this.acesso = this.authSerive.getAcesso();

    this.buscarCalendario();
    var splitted = this.acesso.nome.split(" ");
    var result = ""
    var count = 1
    for (let x of splitted) {
      if(count <= 3){
        if(count == 2 && x.length > 3){
          count++
        }
        result = result+ " " + x.toString()
        count++
      }
    }
    this.acesso.nome = result;
  }

  acesso: Acesso;
  items: EventoClienteVO[];
  temp_C: String;
  icon_Temp: String;

  calendarVisible = true;
  calendarOptions: CalendarOptions = {
    headerToolbar: {
      left: 'title', //today
      center: 'prev,next',
      right: '' //dayGridMonth,timeGridWeek,timeGridDay,listWeek
    },
    height: 'auto',
    contentHeight: 'auto',
    aspectRatio: 10,
    initialView: 'dayGridMonth',
    themeSystem: 'bootstrap',
    locales: allLocales,
    locale: 'pt-br',
    weekends: true,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    timeZone: 'local',
    dayMaxEventRows: true, // for all non-TimeGrid views
    views: {
      timeGrid: {
        dayMaxEventRows: 6 // adjust to 6 only for timeGridWeek/timeGridDay
      }
    },
    handleWindowResize: true
  };

  private obterClima(param: string){

  }

  public buscarCalendario(){
    var city: string = ""
    if (this.cidade.value == null) {
      city = 'Campinas'//this.acesso.local
    }else{
      city = this.cidade.value
    }

    this.dashService.buscarAgendaCliente(this.acesso.email, city).then((dados) => {
      this.items = dados;
      let events = this.items.map((item) => {
          return {
            title: item.eventoNome,
            date: item.eventoData
          }
      })
      this.calendarOptions.events = events
    })
  }

}
