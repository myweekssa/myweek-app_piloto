import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from './servico/auth.service';
import { Acesso } from '../../models/acesso';
import { AcessoVO } from 'src/app/models/vo/acessoVO';
import * as moment from 'moment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private auth : AuthService,
    private router : Router
  ) { }

  userLogin : String
  password : String
  acesso: Acesso;
  userLogado: AcessoVO;

  ngOnInit(): void {
  }

  login(){

    this.auth.doLogin(this.userLogin, this.password).then((data) => {
      this.userLogado = data
      this.acesso = new Acesso(this.userLogado.nome, this.userLogado.email, this.userLogado.local);
      this.acesso.acessadoEm = moment().format("DD/MM/YYYY HH:mm");
      this.acesso.successMessage = 'Login com Sucesso';
      this.acesso.errorMessage = '';

      this.auth.setAcesso(this.acesso)
      this.router.navigate(['/dashboard']);
    })
  }

  cadastro(){
    this.router.navigate(['/cadastro-cliente']);
  }

}
