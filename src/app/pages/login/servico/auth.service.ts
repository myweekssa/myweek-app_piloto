import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

import { Acesso } from '../../../models/acesso';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private urlApi = ''
  SESSION_TOKEN = 'username'

  userLogin: String;
  password: String;

  acesso: Acesso;

  constructor(private http: HttpClient) {
    this.urlApi = environment.apiUrl
  }

  doLogin(userLogin: String, password: String): Promise<any> {

    let acesso = window.btoa(userLogin + ":" + password)
    const headers = new HttpHeaders().set('Authorization', 'Basic ' + acesso);

    return this.http.get(this.urlApi + '/login', { headers, withCredentials: true })
      .toPromise()
  }

  doLogout() {
    sessionStorage.removeItem(this.SESSION_TOKEN);
    this.userLogin = null;
    this.password = null;

    return this.http.get(this.urlApi + '/app/logout', { withCredentials: true }).toPromise()
  }

  getAcesso(): Acesso {
    return this.acesso;
  }

  setAcesso(acesso: Acesso){
    return this.acesso = acesso;
  }
}
