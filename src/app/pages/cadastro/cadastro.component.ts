import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormControl} from '@angular/forms';

import { CadastroService } from './servico/cadastro.service';
import { NovoClienteVO } from './../../models/vo/novo-cliente-vo';
import { Genero } from './../../models/enum/genero.enum';
import { MatDialog } from '@angular/material/dialog';
import { SuccessCadastroDialogComponent } from 'src/app/templete/modals/success-cadastro-dialog/success-cadastro-dialog.component';



@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css'],

  providers: []
})

export class CadastroComponent implements OnInit {

  novoCliente: NovoClienteVO

  generos = new FormControl();
  generoList: string[] =  Object.values(Genero);

  hide = true;
  msg = '';

  constructor(
    private cadService : CadastroService,
    public dialog: MatDialog)
  {
    this.novoCliente = new NovoClienteVO();
  }

  ngOnInit(): void {
    this.msg= ''
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(SuccessCadastroDialogComponent, {
      width: '250px'
    });
  }

  criarCliente(){
    this.novoCliente.genero = this.generos.value
    this.cadService.cadastroNovoCliente(this.novoCliente).then((dados) => {
      let retorno = dados as boolean

      if(retorno){
        this.openDialog()
      }
      
    }, (err) => {
      this.msg = err.data.name
    })
  }

}