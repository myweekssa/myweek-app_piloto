import { NovoClienteVO } from './../../../models/vo/novo-cliente-vo';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class CadastroService {

  private urlApi = ''

  constructor(private http : HttpClient) { 
    this.urlApi = environment.apiUrl
  }

  cadastroNovoCliente(newCliente: NovoClienteVO){
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    
    const body = JSON.stringify(newCliente);
    return this.http.post(this.urlApi + '/api/cadastro/cliente/new', body, {headers, withCredentials: true })
    .toPromise()
  }
}
