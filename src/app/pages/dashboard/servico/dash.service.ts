import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class DashService {

  private urlApi = ''


  constructor(private http : HttpClient) { 
    this.urlApi = environment.apiUrl
  }

  buscarAgendaCliente(email: string, city: string) : Promise<any>{
    return this.http.get(this.urlApi + '/api/calendario/allEvents?email='+email+'&city='+city, { withCredentials: true })
    .toPromise()
  }

  getClima(local: string): Promise<any>{
    return this.http.get(this.urlApi + '/weather?cidade='+local, 
    {responseType:'text', withCredentials: true })
    .toPromise()
  }
}
