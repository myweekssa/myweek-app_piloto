export class NovoClienteVO {
    cliId: string;
    nome: string;
    email: string;
    celular: string;
    dtNascimento: string;

    senha: string;
    novaSenha: string;

    genero: string[];

    constructor() {
        this.cliId= '';
        this.nome= '';
        this.email= '';
        this.celular= '';
        this.dtNascimento= '';

        this.senha= '';
        this.novaSenha= '';

        this.genero= [''];
    }
}
