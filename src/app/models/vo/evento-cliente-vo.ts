export class EventoClienteVO {
    clienteId: string;
    clienteNome: string;
    clienteEmail: string;
    generoNome: string;
    eventoId: string;
    eventoNome: string;
    eventoData: Date;
    eventoStatus: string;

    constructor() {
        this.clienteId = '';
        this.clienteNome = '';
        this.clienteEmail = '';
        this.generoNome = '';
        this.eventoId = '';
        this.eventoNome = '';
        this.eventoData = null;
        this.eventoStatus = '';
    }
}
