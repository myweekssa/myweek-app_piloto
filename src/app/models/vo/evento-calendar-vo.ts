export class EventoCalendarVO {
    id:string;
    title:string;
    start:Date;

    constructor() {
        this.id= '';
        this.title= '';
        this.start= null;
    }
}
