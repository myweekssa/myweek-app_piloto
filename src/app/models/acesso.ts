export class Acesso {
    nome : string;
    email: string;
    local:string;
    errorMessage: string;
    successMessage: string;
    invalidLogin: boolean;
    acessadoEm: string

    constructor(theName: string, theEmail: string, theLocal: string) { 
        this.acessadoEm = '';
        this.nome = theName;
        this.email = theEmail;
        this.local = theLocal;
        this.errorMessage = 'Credenciais Inválidas: Email ou Senha incorreta';
        this.successMessage = '';
        this.invalidLogin = false; }
}
